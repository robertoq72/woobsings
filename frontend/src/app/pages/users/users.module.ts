import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersPageRoutingModule } from './users-routing.module';

import { UsersPage } from './users.page';
import { MaterialModulesModule } from '../../modules/material-modules/material-modules.module';
import { CreateUserModalPage } from '../create-user-modal/create-user-modal.page';
import { CreateUserModalPageModule } from '../create-user-modal/create-user-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsersPageRoutingModule,
    MaterialModulesModule,
    CreateUserModalPageModule
  ],
  declarations: [UsersPage],
  entryComponents: [],
})
export class UsersPageModule {}
