import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UsersService } from '../../providers/users.service';

@Component({
  selector: 'app-create-user-modal',
  templateUrl: './create-user-modal.page.html',
  styleUrls: ['./create-user-modal.page.scss'],
})
export class CreateUserModalPage implements OnInit {
  public data = {
    firstname: '',
    lastname: '',
    phone: '',
    email: '',
    address: '',
  };
  constructor(
    public modalController: ModalController,
    public userServices: UsersService
    ) { }

  ngOnInit() {
  }
  closeModal() {
    this.modalController.dismiss();
  }
  public create() {
    this.userServices.createUser(this.data);
  }
}
